'use strict';
const bcrypt = require('bcrypt');
const uuid = require('uuid/v4');
module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
   return queryInterface.bulkInsert('admins', [{
     id: uuid(),
     name: 'admin',
     username: 'admin',
     password: bcrypt.hashSync('admin', bcrypt.genSaltSync()),
     created_at: new Date(),
     updated_at: new Date(),
   }])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
