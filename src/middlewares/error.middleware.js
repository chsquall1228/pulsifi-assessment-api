const ArgumentError = require('../../src/errors/arguement.error');
const AuthError = require('../../src/errors/auth.error');
const NotFoundError = require('../../src/errors/notfound.error');
const UnprocessableError = require('../../src/errors/unprocessable.error')

exports.errorHandle = (error, req, res, next) => {
    console.error(error.message);
    let status = 500;
    if(error instanceof ArgumentError){
        status = 400;
    }else if(error instanceof AuthError){
        status = 401;
    }else if(error instanceof NotFoundError){
        status = 404;
    }else if(error instanceof UnprocessableError){
        status = 422;
    }
    return res.status(status).send({"message": error.message});
};