const jwt = require('jsonwebtoken');

exports.checkToken = (req, res, next) => {
    let token = req.headers['authorization'];
    if(token && token.startsWith('Bearer ')){
        token = token.slice(7);
    }

    if(token){
        jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
            if(err) {
                return res.status(401).send({
                    message: 'Invalid token'
                })
            }else{
                req.user = decoded.user;
                next();
            }
        })
    }else{
        return res.status(401).send({
            message: 'Unauthorize access'
        })
    }
}   