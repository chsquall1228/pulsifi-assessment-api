const express = require('express');
const authRoutes = require('./auth.route');
const jobRoutes = require('./job.route');
const adminRoutes = require('./admin.route');

const router = express.Router();

router.use('/auth', authRoutes);
router.use('/admins', adminRoutes);
router.use('/jobs', jobRoutes);

module.exports = router;
