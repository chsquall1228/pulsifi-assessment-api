const express = require('express');
const apiRoutes = require('./api/index.route');

const router = express.Router();
router.use('/api', apiRoutes);
module.exports = router;
