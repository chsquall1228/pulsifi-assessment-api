const sinon = require('sinon');
const Controller = require('../../../../src/controllers/api/v1/base.controller')
const NotFoundError = require('../../../../src/errors/notfound.error')
const AuthError = require('../../../../src/errors/auth.error')
const {Admin} = require('../../../../src/models/index');
const sinonTest = require('sinon-test');
const test = sinonTest(sinon);

describe('Base Controller', function(){
    describe('pagination', function(){
        beforeEach(function(){
            next = sinon.spy();
        });
    
        it('should return default limit model', test(async function(){
            let model = {
                findAll : sinon.stub().resolves([{"username": 'admin'},{"username": 'admi2'},{"username": 'admi3'}]),
                count: sinon.stub().resolves(3)
            };
            let req = {};
            let response = await Controller.pagination(model, req);
            sinon.assert.calledWith(model.findAll, sinon.match.has('limit', 50 ));
            sinon.assert.match(response.size, 3);
        }));
        
        it('should return unlimited limit model', test(async function(){
            let model = {
                findAll : sinon.stub().resolves([{"username": 'admin'},{"username": 'admi2'},{"username": 'admi3'}]),
                count: sinon.stub().resolves(3)
            };
            let req = {query: {limit : -1}};
            let response = await Controller.pagination(model, req);
            sinon.assert.calledWith(model.findAll, sinon.match({}));
            sinon.assert.match(response.size, 3);
        }));
        
        it('should return unlimited limit model', test(async function(){
            let model = {
                findAll : sinon.stub().resolves([{"username": 'admin'},{"username": 'admi2'},{"username": 'admi3'}]),
                count: sinon.stub().resolves(3)
            };
            let req = {query: {limit : 20}};
            let response = await Controller.pagination(model, req);
            sinon.assert.calledWith(model.findAll, sinon.match.has('limit', 20));
            sinon.assert.match(response.size, 3);
        }));
    });

    describe('getResource', function(){
        it('should return not found error', test(async function(){
            let model = {
                findOne: sinon.stub().resolves(null)
            };
            try
            {
                await Controller.getResource(model, 'fake');
            }catch(error){
                sinon.assert.match(error.name, 'NotFoundError');
            }
        }));

        it('should return resource', test(async function(){
            let resource = {'username': 'test'};
            let model = {
                findOne: sinon.stub().resolves(resource)
            };
            let response = await Controller.getResource(model, 'test');
            sinon.assert.match(response, resource)
        }));
    })
})