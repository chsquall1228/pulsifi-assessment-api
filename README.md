# PulsifiAssessmentAPI

nodejs, express framework

## default config file as .env
PORT=80

SECRET_KEY=pulsifi

NODE_ENV=production

## Development server
Run `npm run start` to start a development server

## Unit Test
Run `npm run test` to start unit testing checking

## Production server
Run `npm run prod` to start a production server
