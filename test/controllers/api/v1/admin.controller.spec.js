const sinon = require('sinon');
const Controller = require('../../../../src/controllers/api/v1/admins.controller')
const base = require('../../../../src/controllers/api/v1/base.controller')
const ArgumentError = require('../../../../src/errors/arguement.error')
const AuthError = require('../../../../src/errors/auth.error')
const { Admin } = require('../../../../src/models/index');
const sinonTest = require('sinon-test');
const test = sinonTest(sinon);

describe('Admin Controller', function(){
    beforeEach(function(){
        res = {
            json: sinon.spy(),
            status: sinon.stub().returns({send: sinon.spy()}),
            send: sinon.spy()
        }

        next = sinon.spy();
    });

    describe('index', function(){
    
        it('should return list', test(async function(){
            let req = {};
            let json = {items: [], size: 0};
            this.stub(base, 'pagination').resolves(json);
            await Controller.index(req, res, next);
            sinon.assert.calledWith(res.send, json);
        }));
    });

    describe('get', function(){
        it('should return resource', test(async function(){
            let req = {params: { id: 'admin'}};
            let json = {"username": 'admin'}
            this.stub(base, 'getResource').resolves(json);
            await Controller.get(req, res, next);
            sinon.assert.calledWith(base.getResource, sinon.match.any, sinon.match('admin'), sinon.match.any);
            sinon.assert.calledWith(res.send, json);
        }));
    });

    describe('post', function(){
        it('should return error without username', test(async function(){
            let req = {body: {}};
            await Controller.post(req, res, next);
            sinon.assert.calledWith(next, sinon.match.has('name', 'ArgumentError'));
        }));

        it('should return error without password', test(async function(){
            let req = {body: {username: 'admin'}};
            await Controller.post(req, res, next);
            sinon.assert.calledWith(next, sinon.match.has('name', 'ArgumentError'));
        }));

        it('should return error same username', test(async function(){
            let req = {body: {username: 'admin', password: 'admin'}};
            this.stub(Admin, 'count').resolves(1);
            await Controller.post(req, res, next);
            sinon.assert.calledWith(next, sinon.match.has('name', 'UnprocessableError'));
        }))
        
        it('should return success', test(async function(){
            let req = {body: {username: 'admin', password: 'admin'}};
            this.stub(Admin, 'create').resolves({id: 'success'});
            this.stub(Admin, 'count').resolves(0);
            await Controller.post(req, res, next);
            sinon.assert.calledWith(Admin.create, req.body);
            sinon.assert.calledWith(res.status, 201);
            sinon.assert.calledWith(res.status(201).send, sinon.match.has('id', 'success'));
        }));
    });

    describe('put', function(){
        it('should return error same username', test(async function(){
            let req = {body: {username: 'admin2', password: ''}, params: {id: 'admin'}};
            this.stub(base, 'getResource').resolves({username: 'admin'});
            this.stub(Admin, 'count').resolves(1);
            await Controller.put(req, res, next);
            sinon.assert.calledWith(next, sinon.match.has('name', 'UnprocessableError'));
        }));

        it('should return success', test(async function(){
            let req = {body: {username: 'admin', password: ''}, params: {id: 'admin'}};
            let json = {"username": 'admin', update: sinon.spy()}
            this.stub(Admin, 'count').resolves(0);
            this.stub(base, 'getResource').resolves(json);
            await Controller.put(req, res, next);
            sinon.assert.calledWith(json.update, sinon.match.has('username', 'admin'));
            sinon.assert.calledOnce(res.send);
        }))
    });

    describe('delete', test(async function(){
        it('should return success', test(async function(){
            let req = {params: {id: 'success'}};
            let json = {"username": 'admin', destroy: sinon.spy()}
            this.stub(base, 'getResource').resolves(json);
            await Controller.delete(req, res, next);
            sinon.assert.calledWith(base.getResource, sinon.match.any, sinon.match('success'));
            sinon.assert.calledOnce(json.destroy);
            sinon.assert.calledOnce(res.send);
        }));
    }))
})