const { Admin } = require('../../../models');
const ArgumentError = require('../../../errors/arguement.error');
const UnprocessableError = require('../../../errors/unprocessable.error');
const base = require('./base.controller');
const {Op} = require('sequelize');

exports.index = async (req, res, next) => {
    try
    {
        let options = {
            attributes: {
                exclude: ['password']
            }
        }
        let response = await base.pagination(Admin, req, options);
        return res.send(response);
    }catch(error){
        next(error);
    }
};

exports.get = async (req, res, next) => {
    try
    {
        let response = await base.getResource(Admin, req.params.id, {
            attributes: {
                exclude: ['password']
            }
        });
        return res.send(response);
    }catch(error){
        next(error);
    }
};

exports.post = async (req, res, next) => {
    try
    {
        let admin = req.body;

        if(!admin.username){
            throw new ArgumentError('Missing Username');
        }
    
        if(!admin.password){
            throw new ArgumentError('Missing Password');
        }

        if((await Admin.count({where: { username: admin.username}})) > 0){
            throw new UnprocessableError('username has been exist');
        }
    
        admin = await Admin.create(admin);
    
        return res.status(201).send({id: admin.id});
    }catch(error){
        next(error);
    }
};

exports.put = async (req, res, next) => {
    try
    {
        let admin = req.body;
        if('password' in admin && !admin.password){
            delete admin['password'];
        }

        if(admin.username && (await Admin.count({where: { username: admin.username, id: { [Op.not]: req.params.id}}})) > 0){
            throw new UnprocessableError('username has been exist');
        }

        admin = await base.getResource(Admin, req.params.id);
        await admin.update(admin);
        return res.send();
    }catch(error){
        next(error);
    }
};


exports.delete = async (req, res, next) => {
    try
    {
        var admin = await base.getResource(Admin, req.params.id);
        await admin.destroy();
        return res.send();
    }catch(error){
        next(error);
    }
};
