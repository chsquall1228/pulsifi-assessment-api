const NotFoundError = require('../../../errors/notfound.error');

exports.pagination = async (model, req, options = {}) => {
    if(req.query && req.query.offset){
        options['offset'] = parseInt(req.query.offset)
    }

    if(req.query && req.query.limit){
        if(req.query.limit > 0){
            options['limit'] = parseInt(req.query.limit)
        }
    }else{
        options['limit'] = 50;
    }
    
    var size = await model.count(options);
    var items = await model.findAll(options);
    return {
        items,
        size
    };
}

exports.getResource = async(model, id, options = {}) => {
    if(options.where){
        options.where.id = id;
    }else{
        options.where = { 'id': id};
    }
    let resource = await model.findOne(options);

    if(!resource){
        throw new NotFoundError();
    }
    return resource;
}
