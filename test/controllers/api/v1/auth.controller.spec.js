const sinon = require('sinon');
const Controller = require('../../../../src/controllers/api/v1/auth.controller')
const ArgumentError = require('../../../../src/errors/arguement.error')
const AuthError = require('../../../../src/errors/auth.error')
const {Admin} = require('../../../../src/models/index');
const sinonTest = require('sinon-test');
const test = sinonTest(sinon);

describe('Auth Controller', function(){
    describe('post Login', function(){
        beforeEach(function(){
            res = {
                json: sinon.spy(),
                status: sinon.stub().returns({send: sinon.spy()}),
            }
    
            next = sinon.spy();
        });
    
        it('should return missing argument error', test(function(){
            let req = {body: {}};
            Controller.postLogin(req, res, next);
            sinon.assert.calledWith(next, sinon.match.instanceOf(ArgumentError));
        }));
    
        it('should return authentication error which admin not found', test( async function(){
            let req = { body: {username: "invalid", password: 'invalid'}};
            this.stub(Admin,'findOne').resolves(null);
            await Controller.postLogin(req, res, next);
            sinon.assert.calledWith(Admin.findOne, sinon.match.hasNested('where.username', req.body.username));
            sinon.assert.calledWith(next, sinon.match.instanceOf(AuthError));
        }));
        
        it('should return authentication error which admin', test(async function(){
            let req = { body: {username: "invalid", password: 'invalid'}};
            this.stub(Admin,'findOne').resolves({validatePassword: function(){ return false}});
            await Controller.postLogin(req, res, next);
            sinon.assert.calledWith(Admin.findOne, sinon.match.hasNested('where.username', req.body.username));
            sinon.assert.calledWith(next, sinon.match.instanceOf(AuthError));
        }));
        
        it('should return token', test(async function(){
            let req = { body: {username: "test", password: 'test'}};
            this.stub(Admin,'findOne').resolves({username: 'admin', validatePassword: function(fake){ return true}});
            process.env.SECRET_KEY = 'test';
            await Controller.postLogin(req, res, next);
            sinon.assert.calledWith(Admin.findOne, sinon.match.hasNested('where.username', req.body.username));
            sinon.assert.calledWith(res.status, 200);
            sinon.assert.calledWith(res.status(200).send, sinon.match.hasNested('admin.username', 'admin').and(sinon.match.has('access_token')))
        }));
    })
})