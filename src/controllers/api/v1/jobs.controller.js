const { Job } = require('../../../models');
const ArgumentError = require('../../../errors/arguement.error');
const NotFoundError = require('../../../errors/notfound.error');
const UnprocessableError = require('../../../errors/unprocessable.error');
const base = require('./base.controller');
const {Op} = require('sequelize');
const multer = require('multer');
const mkdirp = require('mkdirp');
const fs = require('fs');
const path = require('path');

exports.index = async (req, res, next) => {
    try
    {
        return res.send(await base.pagination(Job, req));
    }catch(error){
        next(error);
    }
};

exports.get = async (req, res, next) => {
    try
    {
        let job = (await base.getResource(Job, req.params.id)).toJSON();
        if(fs.existsSync(`storage/jobs/${job.id}/file.pdf`)){
            job.file = `/api/v1/jobs/${job.id}/file`;
        }
        return res.send(job);
    }catch(error){
        next(error);
    }
};

exports.post = async (req, res, next) => {
    try
    {
        let job = req.body;
        if(!job.title){
            throw new ArgumentError("Missing title");
        }

        if('id' in job){
            delete job.id;
        }

        if((await Job.count({where: { title: job.title}})) > 0){
            throw new UnprocessableError('Job title has been exist');
        }

        job = await Job.create(job);
        return res.status(201).send({id: job.id});
    }catch(error){
        next(error);
    }
};

exports.put = async (req, res, next) => {
    try
    {
        let job = req.body;
        
        if(job.title && (await Job.count({where: { title: job.title, id: { [Op.not]: req.params.id}}})) > 0){
            throw new UnprocessableError('Job title has been exist');
        }

        job = await base.getResource(Job, req.params.id);
        await job.update(req.body);
        return res.send({id: job.id});
    }catch(error){
        next(error);
    }
};


exports.delete = async (req, res, next) => {
    try
    {
        var job = await base.getResource(Job, req.params.id);
        await job.destroy();
        return res.send();
    }catch(error){
        next(error);
    }
};

exports.postFile = async(req, res, next) => {
    try
    {
        await base.getResource(Job, req.params.id);
        var upload = multer({dest: 'storage/tmp', limits: { fileSize: 2000000}}).single('file');
        upload(req, res, function(err){
            try
            {
                if(err){
                    throw err;
                }
                let folderPath = `storage/jobs/${req.params.id}`;
                mkdirp.sync(folderPath);
                let filepath = `${folderPath}/file.pdf`;
                fs.renameSync(req.file.path, filepath);
                return res.status(201).send({"message": "File is uploaded"});
            }catch(error){
                next(error);
            }
        })
    }catch(error){
        next(error);
    }
}

exports.getFile = async(req, res, next) => {
    try
    {
        let filepath = `storage/jobs/${req.params.id}/file.pdf`;
        if(!fs.existsSync(filepath)){
            throw new NotFoundError("File not found");
        }
        return res.sendFile(path.resolve(filepath));
    }catch(error){
        next(error);
    }
}
