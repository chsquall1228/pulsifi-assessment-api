const dotenv = require('dotenv');
dotenv.config();

const express = require('express')
const indexRouter = require('./routes/index.route');
const errorMiddleware = require('./middlewares/error.middleware');
const cors = require('cors');
const path = require('path');


const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(express.static(path.resolve('public')));
app.use(/^\/$/, function(req, res){
    res.redirect('/app');
})
app.use('/app', function(req, res){
  res.sendFile(path.resolve('public/app/index.html'))
});

app.use('/', indexRouter);

app.use(errorMiddleware.errorHandle);

app.listen(process.env.PORT, '0.0.0.0', () => console.log(`Example app listening on port ${process.env.PORT}!`));
module.exports = app;