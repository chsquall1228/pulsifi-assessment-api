var express = require('express');
var adminController = require('../../../controllers/api/v1/admins.controller');
var router = express.Router();
var authMiddleware = require('../../../middlewares/auth.middleware');

/* GET users listing. */
router.get('/', authMiddleware.checkToken, adminController.index);
router.post('/', authMiddleware.checkToken, adminController.post);
router.get('/:id', authMiddleware.checkToken, adminController.get);
router.put('/:id', authMiddleware.checkToken, adminController.put);
router.delete('/:id', authMiddleware.checkToken, adminController.delete);

module.exports = router;
