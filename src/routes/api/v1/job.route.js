var express = require('express');
var jobsController = require('../../../controllers/api/v1/jobs.controller');
var router = express.Router();
var authMiddleware = require('../../../middlewares/auth.middleware');

/* GET users listing. */
router.get('/', jobsController.index);
router.post('/', authMiddleware.checkToken, jobsController.post);
router.get('/:id', jobsController.get);
router.put('/:id', authMiddleware.checkToken, jobsController.put);
router.delete('/:id', authMiddleware.checkToken, jobsController.delete);
router.post('/:id/file', authMiddleware.checkToken, jobsController.postFile);
router.get('/:id/file', jobsController.getFile);

module.exports = router;
