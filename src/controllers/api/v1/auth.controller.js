const { Admin } = require('../../../models');
const ArgumentError = require('../../../errors/arguement.error');
const AuthError = require('../../../errors/auth.error');
const jwt = require('jsonwebtoken');

exports.postLogin = async (req, res, next) => {
    try
    {
        if(!req.body.username || !req.body.password){
            throw new ArgumentError('Username/Password is required');
        }
    
        let admin = await Admin.findOne({
            where: {
                username: req.body.username
            }
        });

        if(!admin || !admin.validatePassword(req.body.password)){
            throw new AuthError('Username/Password mismatch');
        }
    
        let expiresIn = 24 * 60 *60;
        const accessToken = jwt.sign({admin}, process.env.SECRET_KEY , {
            expiresIn: expiresIn
        });
        return res.status(200).send({ admin: admin, access_token : accessToken});
    }catch(error){
        return next(error);
    }
}