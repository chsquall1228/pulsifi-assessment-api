const sinon = require('sinon');
const jwt = require('jsonwebtoken');
const Middleware = require('../../src/middlewares/auth.middleware');
const sinonTest = require('sinon-test');
const test = sinonTest(sinon);

describe('Auth Middleware', function(){
    describe('check Token', function(){
        beforeEach(function(){
            res = {
                json: sinon.spy(),
                status: sinon.stub().returns({send: sinon.spy()}),
            }
    
            next = sinon.spy();
        });
        
        it('should return unauthenticate without token', test(function(){
            let req = {headers: {}};
            Middleware.checkToken(req, res, next);
            sinon.assert.calledWith(res.status, 401);
        }));

        it('should return unauthenticate fake token', test(function(){
            let req = {headers: { 'authorization': 'fake'}}
            this.stub(jwt, 'verify').yields('is fake', null);
            Middleware.checkToken(req, res, next);
            sinon.assert.calledWith(res.status, 401);
            sinon.assert.calledWith(res.status(401).send, sinon.match.has('message', 'Invalid token'));
        }));

        it('should proceed next', test(function(){
            let req = {headers: { 'authorization': 'ok'}};
            this.stub(jwt, 'verify').yields(null, {user: {username: 'admin'}});
            Middleware.checkToken(req, res, next);
            sinon.assert.calledWith(next);
        }));
    });
});