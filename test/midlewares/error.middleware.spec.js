const sinon = require('sinon');
const Middleware = require('../../src/middlewares/error.middleware');
const ArgumentError = require('../../src/errors/arguement.error');
const AuthError = require('../../src/errors/auth.error');
const NotFoundError = require('../../src/errors/notfound.error');
const UnprocessableError = require('../../src/errors/unprocessable.error');
const sinonTest = require('sinon-test');
const test = sinonTest(sinon);

describe('Error Middleware', function(){
    describe('error Handle', function(){
        beforeEach(function(){
            res = {
                json: sinon.spy(),
                status: sinon.stub().returns({send: sinon.spy()}),
            }
            req = {}
            next = sinon.spy();
        });
        
        it('should return argument error status', test(function(){
            Middleware.errorHandle(new ArgumentError(), req , res, next);
            sinon.assert.calledWith(res.status, 400);
        }));

        it('should return authenticate error status', test(function(){
            Middleware.errorHandle(new AuthError(), req, res, next);
            sinon.assert.calledWith(res.status, 401);
        }));

        it('should return resource not found error status', test(function(){
            Middleware.errorHandle(new NotFoundError(), req, res, next);
            sinon.assert.calledWith(res.status, 404);
        }));

        it('should return server error', test(function(){
            Middleware.errorHandle(new Error(), req, res, next);
            sinon.assert.calledWith(res.status, 500);
        }));
        
        it('should return unprocess error', test(function(){
            Middleware.errorHandle(new UnprocessableError(), req, res, next);
            sinon.assert.calledWith(res.status, 422);
        }));
    });
});