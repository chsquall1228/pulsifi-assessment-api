class AuthError extends Error{
    constructor(args){
        super(args);
        this.name = "AuthError";
    }
}

module.exports = AuthError;