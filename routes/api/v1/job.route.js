var express = require('express');
var jobsController = require('../../../controllers/api/v1/jobs.controller');
var router = express.Router();
var authMiddleware = require('../../../middlewares/auth.middleware');

/* GET users listing. */
router.get('/', authMiddleware.checkToken, jobsController.index);
router.post('/', authMiddleware.checkToken, jobsController.post);
router.get('/:id', authMiddleware.checkToken, jobsController.get);
router.put('/:id', authMiddleware.checkToken, jobsController.put);
router.delete('/:id', authMiddleware.checkToken, jobsController.delete);

module.exports = router;
