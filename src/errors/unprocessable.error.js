class UnprocessableError extends Error{
    constructor(args){
        super(args);
        this.name = "UnprocessableError";
    }
}

module.exports = UnprocessableError;