const express = require('express');
const authController = require('../../../controllers/api/v1/auth.controller');
const router = express.Router();


/* GET users listing. */
router.post('/login', authController.postLogin);

module.exports = router;