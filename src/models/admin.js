'use strict';
var bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define('Admin', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    }

  }, {
    tableName: 'admins'
  });
  Admin.associate = function(models) {
    // associations can be defined here
  };

  Admin.prototype.validatePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  }

  Admin.beforeCreate((admin, options) => {
    admin.password = bcrypt.hashSync(admin.password, bcrypt.genSaltSync());
  });

  Admin.beforeUpdate((admin, options) => {
    if(admin.password)
    {
      admin.password = bcrypt.hashSync(admin.password, bcrypt.genSaltSync());
    }
  });
  return Admin;
};