'use strict';
const uuid = require('uuid/v4');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('jobs', [{
      id: uuid(),
      title: 'Director',
      description: 'Director Description',
      location: 'Kuala Lumpur',
      date: new Date(),
      status: false,
      created_at: new Date(),
      updated_at: new Date(),
    },{
      id: uuid(),
      title: 'Manager',
      description: 'Manager Description',
      location: 'Kuala Lumpur',
      date: new Date(),
      status: true,
      created_at: new Date(),
      updated_at: new Date(),
    },{
      id: uuid(),
      title: 'Administrator',
      description: 'Administrator Description',
      location: 'Kuala Lumpur',
      date: new Date(),
      status: true,
      created_at: new Date(),
      updated_at: new Date(),
    },{
      id: uuid(),
      title: 'Developer',
      description: 'Developer Description',
      location: 'Kuala Lumpur',
      date: new Date(),
      status: true,
      created_at: new Date(),
      updated_at: new Date(),
    },{
      id: uuid(),
      title: 'Tester',
      description: 'Tester Description',
      location: 'Kuala Lumpur',
      date: new Date(),
      status: true,
      created_at: new Date(),
      updated_at: new Date(),
    }])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
