'use strict';
module.exports = (sequelize, DataTypes) => {
  const Job = sequelize.define('Job', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    title: DataTypes.STRING,
    location: DataTypes.STRING,
    description: DataTypes.STRING,
    date: DataTypes.DATE,
    status: DataTypes.STRING,
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    }
  }, {
    tableName: 'jobs'
  });
  Job.associate = function(models) {
    // associations can be defined here
  };
  
  Job.beforeCreate((job, options) => {
    if(!job.title){
        throw new ArgumentError('Missing Title');
    }

    if(!job.location){
        throw new ArgumentError('Missing Location');
    }

    if(!job.description){
      throw new ArgumentError('Missing Description');
    }
  });
  return Job;
};