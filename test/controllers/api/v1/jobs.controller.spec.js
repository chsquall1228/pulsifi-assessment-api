const sinon = require('sinon');
const Controller = require('../../../../src/controllers/api/v1/jobs.controller')
const base = require('../../../../src/controllers/api/v1/base.controller')
const ArgumentError = require('../../../../src/errors/arguement.error')
const AuthError = require('../../../../src/errors/auth.error')
const { Job } = require('../../../../src/models/index');
const sinonTest = require('sinon-test');
const test = sinonTest(sinon);
const fs = require('fs');

describe('Job Controller', function(){
    beforeEach(function(){
        res = {
            json: sinon.spy(),
            status: sinon.stub().returns({send: sinon.spy()}),
            send: sinon.spy(),
            sendFile: sinon.spy()
        }

        next = sinon.spy();
    });

    describe('index', function(){
        it('should return list', test(async function(){
            let req = {};
            let json = {items: [], size: 0};
            this.stub(base, 'pagination').resolves(json);
            await Controller.index(req, res, next);
            sinon.assert.calledWith(res.send, json);
        }));
    });

    describe('get', function(){
        it('should return resource', test(async function(){
            let req = {params: { id: 'admin'}};
            let json = {toJSON: function () { return { id: 'admin', "title": 'Administrator'}}}
            this.stub(base, 'getResource').resolves(json);
            this.stub(fs, 'existsSync').returns(false);
            await Controller.get(req, res, next);
            sinon.assert.calledWith(base.getResource, sinon.match.any, sinon.match('admin'));
            sinon.assert.calledWith(res.send, json.toJSON());
        }));

        it('should return resource with file', test(async function(){
            let req = {params: { id: 'admin'}};
            let json = {toJSON: function () { return { id: 'admin', "title": 'Administrator'}}}
            this.stub(base, 'getResource').resolves(json);
            this.stub(fs, 'existsSync').returns(true);
            await Controller.get(req, res, next);
            sinon.assert.calledWith(base.getResource, sinon.match.any, sinon.match('admin'));
            sinon.assert.calledWith(res.send, sinon.match.has('title', 'Administrator').and(sinon.match.has('file', '/api/v1/jobs/admin/file')));
        }));
    });

    describe('post', function(){
        it('should return error without title', test(async function(){
            let req = {body: {}};
            this.stub(Job, 'count').resolves(1);
            await Controller.post(req, res, next);
            sinon.assert.calledWith(next, sinon.match.has('name', 'ArgumentError'));
        }));

        it('should return error same title', test(async function(){
            let req = {body: {title: 'Administrator'}};
            this.stub(Job, 'create').resolves({id: 'success'});
            this.stub(Job, 'count').resolves(1);
            await Controller.post(req, res, next);
            sinon.assert.calledWith(next, sinon.match.has('name', 'UnprocessableError'));
        }))
        
        it('should return success', test(async function(){
            let req = {body: {title: 'Administrator'}};
            this.stub(Job, 'create').resolves({id: 'success'});
            this.stub(Job, 'count').resolves(0);
            await Controller.post(req, res, next);
            sinon.assert.calledWith(Job.create, req.body);
            sinon.assert.calledWith(res.status, 201);
            sinon.assert.calledWith(res.status(201).send, sinon.match.has('id', 'success'));
        }));
    });

    describe('put', function(){
        it('should return error same title', test(async function(){
            let req = {body: {title: 'Administrator'}, params: {id: 'admin'}};
            this.stub(Job, 'count').resolves(1);
            await Controller.put(req, res, next);
            sinon.assert.calledWith(next, sinon.match.has('name', 'UnprocessableError'));
        }));

        it('should return success', test(async function(){
            let req = {body: {title: 'Administrator', password: ''}, params: {id: 'admin'}};
            let json = {"title": 'Administrator', update: sinon.spy()}
            this.stub(Job, 'count').resolves(0);
            this.stub(base, 'getResource').resolves(json);
            await Controller.put(req, res, next);
            sinon.assert.calledWith(json.update, sinon.match.has('title', 'Administrator'));
            sinon.assert.calledOnce(res.send);
        }))
    });

    describe('delete', function(){
        it('should return success', test(async function(){
            let req = {params: {id: 'success'}};
            let json = {"title": 'Administrator', destroy: sinon.spy()}
            this.stub(base, 'getResource').resolves(json);
            await Controller.delete(req, res, next);
            sinon.assert.calledWith(base.getResource, sinon.match.any, sinon.match('success'));
            sinon.assert.calledOnce(json.destroy);
            sinon.assert.calledOnce(res.send);
        }));
    });

    describe('getFile', function(){
        it('should return not found', test(async function(){
            let req = { params: {id: 'fail'}};
            this.stub(fs, 'existsSync').returns(false);
            await Controller.getFile(req, res, next);
            sinon.assert.calledWith(fs.existsSync, sinon.match('storage/jobs/fail/file.pdf'));
            sinon.assert.calledWith(next, sinon.match.has('name', 'NotFoundError'));
        }));

        it('should return file', test(async function(){
            let req = { params: {id: 'success'}};
            this.stub(fs, 'existsSync').returns(true);
            await Controller.getFile(req, res, next);
            sinon.assert.calledWith(fs.existsSync, sinon.match('storage/jobs/success/file.pdf'));
            sinon.assert.calledWith(res.sendFile, 'storage/jobs/success/file.pdf');
        }));
    });
})